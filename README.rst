=====================
server-image
=====================

  .. image:: resources/img/logo.png
     :alt: tomato

This repository provides a modified `Raspbian <https://www.raspbian.org/>`_ image that includes the `huertico-server <https://gitlab.com/huertico/huertico-server>`_ setup.

A `bash <https://www.gnu.org/software/bash/>`_ script named **run.sh** is also included to easily run the image as a `qemu <https://www.qemu.org/>`_ virtual machine.

The Raspbian image comes with the following software installed:

- `Mosquitto MQTT broker <https://mosquitto.org/>`_.

- `Node Red IoT <Flow-based programming for the Internet of Things>`_.

And includes configuration for the following software:

- Mosquito MQTT broker:

  - Set `Access Control List <http://www.steves-internet-guide.com/topic-restriction-mosquitto-configuration/>`_ configuration to subscribe and publish using `client id <https://stackoverflow.com/questions/27174271/what-is-the-clientid-needed-for#27176197>`_.

Requirements
============

You need to install the following packages:

 - qemu
 - qemu-img
 - qemu-system-arm
 - qemu-utils

And add your user to the **qemu** group.

Usage
=====

Clone the repository to obtain the files:

.. code-block:: sh

  git clone https://gitlab.com/huertico/server-image
  cd server-image

To execute the Raspbian image as a virtual machine run:

.. code-block:: sh

  ./run.sh

Once running, you can connect via ssh:

.. code-block:: sh

  ssh pi@localhost -p 8082

To put the image into an `SD card <https://en.wikipedia.org/wiki/Secure_Digital>`_ to run on the `Raspberry Pi Zero W <https://www.raspberrypi.org/products/raspberry-pi-zero-w/>`_, first decompress the image and then execute:

.. code-block:: sh

  sudo dd if=raspbian-stretch-huertico.img of=/dev/sdx

Where */dev/sdx* is your SD card device (i.e.: */dev/sdb* or */dev/sdc*).

----------------
Mosquitto (MQTT)
----------------

`Mosquitto <https://mosquitto.org/>`_ is a lightweight `MQTT <http://mqtt.org/>`_ broker.

When a qemu virtual machine is running, you can subscribe and publish to the Mosquitto broker, for example:

To subscribe as the client **c2** to the client **c1** channel:

.. code-block:: sh

  mosquitto_sub -i c2 -t 'huertico/c1/data' -p 8081

To publish as client **c1**:

.. code-block:: sh

  mosquitto_pub -d -i c1 -t huertico/c1/data -p 8081 \
    -m '{"id":"c1","type":"earth","data":{"light":956,"moisture":750,"humidity":39,"temperature":20}}'

--------
Node-RED
--------

`Node-RED <https://nodered.org/>`_ is a programming tool for the `Internet of Things <https://en.wikipedia.org/wiki/Internet_of_Things>`_ (IoT).

To access the Node-RED running on the virtual machine from the physical machine, open a web browser and go to the address *localhost:8080* or from a terminal you can run:

.. code-block:: sh

  firefox localhost:8080

Port Redirection
================

When running the image as qemu virtual machine (using **run.sh**), the following ports are redirected from the virtual machine to the physical machine:

- *NODE-RED*: Port *1880* used by **Node-RED** is redirected to *8080*, for example, accesing with `firefox <https://www.mozilla.org/en-US/firefox/>`_:

  .. code-block:: sh

    firefox localhost:8080

- *MQTT*: Port *1883* is redirected to *8081*, for example, subscribing from the physical machine:

  .. code-block:: sh

    mosquitto_sub -i c2 -t 'huertico/c1/data' -p 8081

- *SSH*: Port *22* is redirected to *8082*, for example, accesing from a terminal:

  .. code-block:: sh

    ssh pi@localhost -p 8082

Compatibility
=============

- `Debian stretch <https://wiki.debian.org/DebianStretch>`_.

- `AMD 64 <https://en.wikipedia.org/wiki/X86-64>`_.

- `ARM 64 <https://en.wikipedia.org/wiki/ARM_architecture>`_.

License
=======

GPL 3. See the `LICENSE <https://gitlab.com/huertico/server-image/blob/master/LICENSE>`_ file for more details.

Authors Information
===================

This repository was created by `constrict0r <https://geekl0g.wordpress.com/author/constrict0r/>`_ and `valarauco <https://twitter.com/valarauco>`_.

  .. image:: resources/img/constrict0r.png
     :target: https://geekl0g.wordpress.com/author/constrict0r
     :alt: constrict0r

  .. image:: resources/img/valarauco.png
     :target: https://twitter.com/valarauco
     :alt: valarauco

Enjoy!!!

  .. image:: resources/img/enjoy.png
     :alt: enjoy!!!
