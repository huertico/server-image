#!/bin/bash
#
# Run the Raspbian image as a qemu virtual machine.

IMAGE_PATH=resources/raspbian-stretch-huertico

# If uncompressed Rasbpian image not found, search for compressed Raspbian image.
if ! [[ -f ${IMAGE_PATH}.img ]]; then

    # If compressed image not found, show error and exit.
    if ! [[ -f ${IMAGE_PATH}.img.tar.xz ]]; then
        echo "Error: the file ${IMAGE_PATH}.img.tar.xz does not exists, aborting ..."
        exit 1

    # Decompress image.
    else
        echo "Uncompressing Raspbian image ..."
        cp ${IMAGE_PATH}.img.tar.xz ${IMAGE_PATH}-extract.img.tar.xz
        unxz ${IMAGE_PATH}-extract.img.tar.xz && tar xf ${IMAGE_PATH}-extract.img.tar -C resources
        rm ${IMAGE_PATH}-extract.img.tar &>/dev/null
    fi
    
fi

# Run virtual machine.
sudo qemu-system-arm -smp 1 -M versatilepb -m 256 -cpu arm1176 \
                -hda ${IMAGE_PATH}.img \
                -net nic \
                -net user,hostfwd=tcp::8080-:1880,hostfwd=tcp::8081-:1883,hostfwd=tcp::8082-:22 \
                -dtb resources/versatile-pb.dtb \
                -kernel resources/kernel-qemu-4.14.79-stretch \
                -append 'root=/dev/sda2 panic=1' \
                -no-reboot

exit 0
